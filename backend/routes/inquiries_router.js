const express = require("express");

const InquiryRouter = express.Router();

const InquiryModel = require("../models/Inquiry");

InquiryRouter.post("/addinquiry", async (req, res) => {
  try {
    let inquiry = InquiryModel({
      code: req.body.code,
      population: req.body.population,
      totalHours: req.body.totalHours,
      logs: [
        {
          message: "Initial Inquiry"
        }
      ],
      initialBookDate: req.body.initialBookDate,
      initialBookHourFrom: req.body.initialBookHourFrom,
      initialBookHourTo: req.body.initialBookHourTo,
      totalPayment: req.body.totalPayment,
      areaName: req.body.areaName,
      areaId: req.body.areaId,
      inquirer: req.body.inquirer,
      inquirerId: req.body.inquirerId,
      approver: req.body.approver,
      approverId: req.body.approverId
    });

    inquiry = await inquiry.save();
    res.send(inquiry);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.get("/showinquiries", async (req, res) => {
  try {
    let inquiries = await InquiryModel.find();
    res.send(inquiries);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.get("/showinquirybyid/:id", async (req, res) => {
  try {
    let inquiry = await InquiryModel.findById(req.params.id);
    res.send(inquiry);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.get("/showinquiriesbyareaid/:id", async (req, res) => {
  try {
    let inquiry = await InquiryModel.find({ areaId: req.params.id });
    res.send(inquiry);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.put("/updateinquiry/:id", async (req, res) => {
  try {
    let inquiry = await InquiryModel.findById(req.params.id);

    if (!inquiry) {
      return res.status(404).send(`Inquiry can't be reached`);
    }

    let id = { _id: req.params.id };
    let updates = {
      code: req.body.code,
      status: req.body.status,
      population: req.body.population,
      inquiryHours: req.body.inquiryHours,
      logs: [
        {
          message: "Updated Inquiry"
        }
      ],
      areaName: req.body.areaName,
      areaId: req.body.areaId,
      inquirer: req.body.inquirer,
      inquirerId: req.body.inquirerId,
      approver: req.body.approver,
      approverId: req.body.approverId
    };

    let updatedInquiry = await InquiryModel.findOneAndUpdate(id, updates, {
      new: true
    });
    res.send(updatedInquiry);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.delete("/deleteinquiry/:id", async (req, res) => {
  try {
    let inquiry = await InquiryModel.findByIdAndDelete(req.params.id);
    res.send(inquiry);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.patch("/updateinquirystatus/:id", async (req, res) => {
  try {
    let oldLogs = await InquiryModel.findById(req.params.id);
    let logs = oldLogs.logs;
    logs.push(req.body.logs);

    let id = { _id: req.params.id };
    let update = {
      status: req.body.status,
      approver: req.body.approver,
      approverId: req.body.approverId,
      logs: logs
    };
    let updated = await InquiryModel.findOneAndUpdate(id, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.patch("/updatepopulation/:id", async (req, res) => {
  try {
    let oldLogs = await InquiryModel.findById(req.params.id);
    let logs = oldLogs.logs;
    logs.push(req.body.logs);

    let id = { _id: req.params.id };
    let update = {
      population: req.body.population,
      logs: logs
    };
    let updated = await InquiryModel.findOneAndUpdate(id, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

InquiryRouter.get("/showinquiriesbyuser/:id", async (req, res) => {
  try {
    let inquiries = await InquiryModel.find({ inquirerId: req.params.id });
    res.send(inquiries);
  } catch (e) {
    console.log(e);
  }
});

module.exports = InquiryRouter;
