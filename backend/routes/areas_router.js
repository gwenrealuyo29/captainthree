const express = require("express");

const AreaRouter = express.Router();

const AreaModel = require("../models/Area");

AreaRouter.post("/addarea", async (req, res) => {
  try {
    let area = AreaModel({
      name: req.body.name,
      description: req.body.description,
      code: req.body.code,
      category: req.body.category,
      availability: req.body.availability,
      price: req.body.price
    });

    area = await area.save();
    res.send(area);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.get("/showareas", async (req, res) => {
  try {
    let areas = await AreaModel.find();
    res.send(areas);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.get("/showareabyid/:id", async (req, res) => {
  try {
    let area = await AreaModel.findById(req.params.id);
    res.send(area);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.put("/updatearea/:id", async (req, res) => {
  try {
    let area = await AreaModel.findById(req.params.id);
    if (!area) {
      return res.status(404).send(`Area can't be found`);
    }
    let id = { _id: req.params.id };
    let updates = {
      name: req.body.name,
      description: req.body.description,
      code: req.body.code,
      category: req.body.category,
      availability: req.body.availability,
      status: req.body.status,
      price: req.body.price
    };

    let updatedArea = await AreaModel.findOneAndUpdate(id, updates, {
      new: true
    });

    res.send(updatedArea);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.delete("/deletearea/:id", async (req, res) => {
  try {
    let deletedArea = await AreaModel.findByIdAndDelete(req.params.id);
    res.send(deletedArea);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.patch("/updateavailability/:id", async (req, res) => {
  try {
    let condition = { _id: req.params.id };
    let update = { availability: req.body.availability };
    let updated = await AreaModel.findOneAndUpdate(condition, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.patch("/updatecategory/:id", async (req, res) => {
  try {
    let condition = { _id: req.params.id };
    let update = { category: req.body.category };
    let updated = await AreaModel.findOneAndUpdate(condition, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.patch("/updatedescription/:id", async (req, res) => {
  try {
    let condition = { _id: req.params.id };
    let update = { description: req.body.description };
    let updated = await AreaModel.findOneAndUpdate(condition, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.patch("/updatename/:id", async (req, res) => {
  try {
    let condition = { _id: req.params.id };
    let update = { name: req.body.name };
    let updated = await AreaModel.findOneAndUpdate(condition, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.patch("/updatestatus/:id", async (req, res) => {
  try {
    let condition = { _id: req.params.id };
    let update = { status: req.body.status };
    let updated = await AreaModel.findOneAndUpdate(condition, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.patch("/updateprice/:id", async (req, res) => {
  try {
    let condition = { _id: req.params.id };
    let update = { price: req.body.price };
    let updated = await AreaModel.findOneAndUpdate(condition, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

AreaRouter.patch("/updatecode/:id", async (req, res) => {
  try {
    let condition = { _id: req.params.id };
    let update = { code: req.body.code };
    let updated = await AreaModel.findOneAndUpdate(condition, update, {
      new: true
    });
    res.send(updated);
  } catch (e) {
    console.log(e);
  }
});

module.exports = AreaRouter;
