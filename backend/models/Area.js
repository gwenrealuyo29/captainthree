const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const AreaSchema = new Schema({
  name: String,
  description: String,
  code: String,
  category: String,
  availability: Number,
  price: Number,
  status: {
    type: String,
    default: "Available"
  }
});

module.exports = mongoose.model("Area", AreaSchema);
