const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const moment = require("moment");

const InquirySchema = new Schema({
  code: String,
  status: {
    type: String,
    default: "Pending"
  },
  population: Number,
  inquiryDate: {
    type: String,
    default: moment(new Date()).format("MM/DD/YY")
  },
  logs: [
    {
      status: {
        type: String,
        default: "Pending"
      },
      updateDate: {
        type: String,
        default: moment(new Date()).format("MM/DD/YY")
      },
      message: String
    }
  ],
  initialBookDate: String,
  initialBookHourFrom: String,
  initialBookHourTo: String,
  totalHours: String,
  totalPayment: Number,
  areaName: String,
  areaId: String,
  inquirer: String,
  inquirerId: String,
  approver: String,
  approverId: String
});

module.exports = mongoose.model("Inquiry", InquirySchema);
