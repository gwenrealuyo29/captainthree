const express = require("express");

const app = express();

const mongoose = require("mongoose");

const config = require("./config");

const cors = require("cors");

const multer = require("multer");

const stripe = require("stripe")("sk_test_rbHPMK3zLn9DDfKuNQ5XtrP2008SKztfDD");

// define storage
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/images/uploads");
  },
  // trasnform filename
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  }
});

const upload = multer({ storage });
const path = require("path");

app.use(express.static(path.join(__dirname, "public")));

mongoose
  .connect(
    "mongodb+srv://admin:hellogwen10@clusterg-epzic.mongodb.net/amapp?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    }
  )
  .then(() => {
    console.log("Remote Database Connection Established");
  });

app.use(express.urlencoded({ extended: false }));

app.use(express.json());
// used for body parsing, transforming data to a readable format

app.use(cors());

app.listen(config.port, () => {
  console.log(`Listening to Port ${config.port}`);
});

const areas = require("./routes/areas_router");

app.use("/admin", areas);

const inquiries = require("./routes/inquiries_router");

app.use("/admin", inquiries);

const users = require("./routes/users_router");

app.use("/", users);

const auth = require("./routes/auth_router");

app.use("/", auth);

app.post("/upload", upload.single("image"), (req, res) => {
  if (req.file) {
    res.json({ imageUrl: `images/uploads/${req.file.filename}` });
  } else {
    res.status("409").json("No files to upload");
  }
});

app.post("/charge", async (req, res) => {
  console.log(req.amount);
  try {
    stripe.customers
      .create({
        email: req.body.email,
        description: req.body.description,
        source: "tok_visa"
      })
      .then(customers =>
        stripe.charges.create({
          amount: req.body.amount,
          currency: "php",
          source: "tok_visa",
          description: req.body.description
        })
      )
      .then(result => console.log(result));
  } catch (e) {
    console.log(e);
  }
});

// app.post("/charge", async (req, res) => {
  //   try{
  //     // let {status} = await stripe.charges.create({
  //     //   amount: 3000,
  //     //   currency: "usd",
  //     //   description: "Sampor",
  //     //   source: req.body
  //     // });
  //     stripe.customers.create({
  //       email: "sample@sample.com",
  //       description: "some sample payment",
  //       source: "tok_visa"
  //     }).then(customers=>
  //       stripe.charges.create({
  //         amount: req.body.amount,
  //         currency: "php",
  //         description: "Sampor",
  //         source: "tok_visa"
  //       });
  //     ).then(result=>{
  //       console.log(result)
  //     })
  //     // res.json({status});
  //   } catch (e) {
  //     console.log(e)
  //   }
// });
